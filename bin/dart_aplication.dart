import 'dart:ffi';

/*
Здравствуйте ментор, как у вас жизнь, как дела, как работа, 
меня зовут Муратов Арген, мне 17 лет и я учусь в онлайн режиме у вас.
Мне все нравится, учусь по тихоньку, конечно в online чуть скучнее
чем в offline но не менее интереснее, поэтому не жалуюсь,
уроки идут позноавательно и интересно,
но я не включаю микрофон и не разговариваю, 
не хочу вам мешать, у вас и так там в offline много мароки.
Хотел сказать что я не пропускал занятия, только вот в прошлую 
пьятницу один раз пропустил!
спасибо вам огромное!
*/
void main(List<String> arguments) {
  //Прошлое домашнее заданиею.
  /*
  int a = 3;
  double b = 4.20;

  print(a + b);

  String myText = 'b';
  double myNum = 2.225;
  myText = (myNum.toString());

  print(myText);

  String text = ('Name');
  text = ('Flutter');

  print('I want to learn ' + text);
*/

//firstHomework
  print('-----------start----------');
  final String address = 'Suumbaeva st., Bishkek';

  print(
      'Длинна предложения "Suumbaeva st., Bishkek":\nровно ${address.length} символа.');

  print('------------------------');

//secondHomework

  int age = 17;
  int weight = 59;
  String name = 'Argen';

  print('Возраст: $age, вес: $weight, имя: ${name.toUpperCase()}');

  print('------------------------');

  //thirdHomework

  int ab = 20;
  String bb = '241292';

  ab = (int.parse(bb));

  print(ab);

  print('-----------end----------');
}
